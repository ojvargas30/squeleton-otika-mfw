<?php
//ctrl significa controller en este caso

require 'app/init.php';

error_reporting(E_ALL);

if (!isset($_REQUEST['c'])) {

    $ctrl = 'AuthController';

    require URL_APP . 'controllers' . SEP . $ctrl . '.php';

    $ctrl = new $ctrl;

    $ctrl->index();
} else {
    $ctrl = ucwords(strtolower($_REQUEST['c'])) . 'Controller';

    $m = isset($_REQUEST['m']) ? $_REQUEST['m'] : 'index';

    $urlCtrl = URL_APP . 'controllers' . SEP . $ctrl . '.php';

    if (file_exists($urlCtrl)) {
        require $urlCtrl;

        if (class_exists($ctrl)) {
            $ctrl = new $ctrl;

            if (method_exists($ctrl, $m)) call_user_func([$ctrl, $m]);

            else exit('Method of class not exists');
        } else exit('Class of controller not exists');
    } else exit('Controller or file not exists');
}
