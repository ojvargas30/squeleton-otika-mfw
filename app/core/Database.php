<?php

class Database extends PDO
{
    public function __construct()
    {
        try {
            parent::__construct(DB_DRIVER . ":host=" . DB_HOST . "; dbname=" . DB_NAME . "; charset=" . DB_CHARSET, DB_USER, DB_PASSWORD);

            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
        } catch (PDOException $e) {
            echo "Conexión Fallida {$e->getMessage()}";
        }
    }

    /*
		@param $table String
		@param $where Array [key => value]
	*/
    public function getByWhere($table, $where)
    {
        try {
            $column = array_keys($where)[0]; // trae la llave o indice
            $value = array_values($where)[0]; // trae su valor
            $stmt = $this->prepare("SELECT * FROM {$table} WHERE {$column} = :columnvalue");
            $stmt->bindParam(":columnvalue", $value);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_OBJ); // Solo objetos papa!
        } catch (Exception $e) {
            return false;
        }
    }

    public function select($strSql, $arrayData = array(), $fetchMode = PDO::FETCH_OBJ)
    {

        $query = $this->prepare($strSql); // prepara al query

        foreach ($arrayData as $key => $value) { // asigna parametros al query si llegan como un array

            $query->bindParam(":$key", $value);
        }

        if (!$query->execute()) { // valida si se ejecuta o no el query

            echo "La consulta no se realizo";
        } else {

            return $query->fetchAll($fetchMode); // devuelve el objeto del query
        }
    }

    /*
	* @param $table String "example"
	* @param $data Array ej: [fieldName => fieldValue]
	* @param $fetchMode PDO::__const__ OPTIONAL
	*/
    public function selectOne($table, $data = array(), $fetchMode = PDO::FETCH_OBJ)
    {
        $key = array_key_first($data);
        $value = $data[$key];
        $query = $this->prepare("SELECT * FROM {$table} WHERE {$key} = :{$key}");
        $query->bindParam(":{$key}", $value);
        $query->execute();
        return $query->fetch($fetchMode);
    }

    public function insert($table, $data)
    {

        try {
            ksort($data);
            unset($data['controller'], $data['method']);
            $fieldNames = implode('`, `', array_keys($data));
            $fieldValues = ':' . implode(', :', array_keys($data));
            $strSql = $this->prepare("INSERT INTO $table (`$fieldNames`)VALUES ($fieldValues)");

            foreach ($data as $key => $value) {
                $strSql->bindValue(":$key", $value);
            }
            $strSql->execute();
            $this->lastInsertId = $this->lastInsertId();
            return true;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getLastId()
    {
        return $this->lastInsertId;
    }

    public function update($table, $data, $where)
    {
        try {
            ksort($data);
            $fieldDetails = null;
            foreach ($data as $key => $value) {
                $fieldDetails .= "`$key` =:$key,";
            }
            $fieldDetails = rtrim($fieldDetails, ',');

            $strSql = $this->prepare("UPDATE $table SET $fieldDetails WHERE $where");

            foreach ($data as $key => $value) {
                $strSql->bindValue(":$key", $value);
            }
            $strSql->execute();
            return true;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }


    public function delete($table, $where)
    {
        try {
            return $this->exec("DELETE FROM $table WHERE $where");
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }
}
