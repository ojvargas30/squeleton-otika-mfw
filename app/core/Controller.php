<?php
class Controller
{
    protected function reqModel($name)
    {
        $name = ucwords(strtolower($name));

        $path = URL_APP . "models" . SEP . $name . ".php";

        if (file_exists($path)) {

            require $path;

            if (class_exists($name)) {

                return new $name;
            } else exit('Class of controller not exists');
        } else exit('Controller or file not exists');
    }

    protected function reqView($path, $data = [], $renderAll = true)
    {
        extract($data);

        $path = str_replace("." , SEP, $path); // se reemplaza el punto por el separador /

        if($renderAll){
            require_once URL_APP . "views" . SEP . "layouts" . SEP . "header.php";
            require_once URL_APP . "views" . SEP . "layouts" . SEP . "navbar.php";
            require_once URL_APP . "views" . SEP . "layouts" . SEP . "sidebar.php";
            require_once URL_APP . "views" . SEP . "{$path}.php";
            require_once URL_APP . "views" . SEP . "layouts" . SEP . "footer.php";
        }else{
            require_once URL_APP . "views" . SEP . "{$path}.php";
        }
    }

    // public function reqModel($name, $method)
    // {
    //     $name = ucwords(strtolower($name));

    //     $path = URL_APP . "models" . SEP . $name . ".php";

    //     if (file_exists($path)) {

    //         require $path;

    //         if (class_exists($name)) {

    //             $name = new $name;

    //             if (method_exists($name, $method)) call_user_func($name, $method);

    //             else exit('Method of class not exists');
    //         } else exit('Class of controller not exists');
    //     } else exit('Controller or file not exists');
    // }
}
