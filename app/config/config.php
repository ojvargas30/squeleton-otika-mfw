<?php

// bd connection data (6)
if (!defined('DB_HOST')) define("DB_HOST","localhost");
if (!defined('DB_USER')) define("DB_USER","root");
if (!defined('DB_NAME')) define("DB_NAME","solumobil");
if (!defined('DB_PASSWORD')) define("DB_PASSWORD","Otdohas_msj1rhino");
if (!defined('DB_DRIVER')) define("DB_DRIVER","mysql");
if (!defined('DB_CHARSET')) define("DB_CHARSET","utf8");

if (!defined('SEP')) define("SEP","\\"); // separator
if (!defined('URL_APP')) define("URL_APP", dirname(__DIR__). SEP);

if (!defined('TITLE_APP')) define('TITLE_APP', 'Sistema de información by OscarJVD');
if (!defined('MAIN_URL')) define('MAIN_URL', 'http://'.str_replace('index.php','',$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']));


// STYLESHEETS -----------------------------------------------------------

// MAIN
if (!defined('CSS')) define('CSS', MAIN_URL . 'public/assets/css/app.css');
if (!defined('JS')) define('JS', MAIN_URL . 'public/assets/js/app.js');

if (!defined('B4')) define('B4', MAIN_URL . 'public/assets/css/bootstrap.css');

// Plantilla
if (!defined('APP_CSS')) define('APP_CSS', MAIN_URL . 'public/assets/css/app.min.css');
if (!defined('COMPONENTS_CSS')) define('COMPONENTS_CSS', MAIN_URL . 'public/assets/css/components.css');
if (!defined('CUSTOM_CSS')) define('CUSTOM_CSS', MAIN_URL . 'public/assets/css/custom.css');
if (!defined('STYLE_CSS')) define('STYLE_CSS', MAIN_URL . 'public/assets/css/style.css');

if (!defined('APP_JS')) define('APP_JS', MAIN_URL . 'public/assets/js/app.min.JS');
if (!defined('SCRIPTS_JS')) define('SCRIPTS_JS', MAIN_URL . 'public/assets/js/scripts.js');
if (!defined('CUSTOM_JS')) define('CUSTOM_JS', MAIN_URL . 'public/assets/JS/custom.JS');

// DATATABLE
if (!defined('DATATABLE_CSS')) define('DATATABLE_CSS', MAIN_URL . 'public/assets/bundles/datatables/datatables.min.css');
if (!defined('DATATABLE_B4')) define('DATATABLE_B4', MAIN_URL . 'public/assets/bundles/datatables/dataTables.bootstrap4.min.css');

// Plugins o bundles
if (!defined('FAWE')) define('FAWE', MAIN_URL . 'public/assets/font/css/all.css');