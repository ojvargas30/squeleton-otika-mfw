<?php

class Product
{
    private $crud;

    public function __construct()
    {
        try {
            $this->crud = new Database;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getAll()
    {
        try {
            $strSql = "SELECT * FROM Artefacto";
            $query = $this->crud->select($strSql);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    public function getProductById($id_artefacto_PK)
    {
        try {
            $strSql = "SELECT * FROM Artefacto WHERE id_artefacto_PK = :id_artefacto_PK";
            $arrayData = ['id_artefacto_PK' => $id_artefacto_PK];
            $query = $this->crud->select($strSql, $arrayData);
            return $query;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }


	// public function editProduct($arrayDetails, $id_servicio_PK)
	// {
	// 	try {
	// 		$strWhere = 'id_revision_servicio_PK=' . $arrayDetails[0]['id_revision_servicio_PK'];

	// 		foreach ($arrayDetails as $detail) {
	// 			$data = [
	// 				'id_revision_servicio_PK'   => $detail['id_revision_servicio_PK'],
	// 				'id_servicio_FK' 			=> $id_servicio_PK,
	// 				'is_software'  				=> $detail['is_software'],
	// 				'is_hardware'  				=> $detail['is_hardware'],
	// 				'descripcion_cliente'  		=> $detail['descripcion_cliente'],
	// 				'diagnostico'  				=> $detail['diagnostico'],
	// 				'costo_revision'  			=> $detail['costo_revision'],
	// 				'costo_domicilio'         	=> $detail['costo_domicilio'],
	// 				'costo_total'         		=> $detail['costo_total'] + $detail['costo_revision'] + $detail['costo_domicilio'],
	// 				'fecha_encuentro'  			=> $detail['fecha_encuentro'],
	// 				'hora_encuentro'  			=> $detail['hora_encuentro'],
	// 				'direccion_lugar'  			=> $detail['direccion_lugar']
	// 			];

	// 			$this->pdo->update('Revision_Servicio', $data, $strWhere);
	// 		}
	// 		return true;
	// 	} catch (PDOException $e) {
	// 		return $e->getMessage();
	// 	}
	// }
}
