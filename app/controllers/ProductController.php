<?php

class ProductController extends Controller
{
    private $model;

    public function __construct()
    {
        $this->model = $this->reqModel("product");

    }

    public function index()
    {
        $products = $this->model->getAll();

        return $this->reqView("product.list",["products" => $products]);
    }

    public function edit()
    {
        if(isset($_REQUEST['id'])){

            $products = $this->model->getProductById($_REQUEST['id']);
            // dd($products);
            return $this->reqView("product.edit",["products" => $products]); // no lleva los datos
        }else{
            echo "El id no llegó";
        }
    }

    public function update()
    {
        $arrayResp = [];

        if (isset($_POST)) {

            $dataService = // DATOS SERVICIO
                [
                    'id_servicio_PK'         =>  $_POST['id_servicio_PK'],
                    'id_cliente_FK'          =>  $_POST['id_cliente_FK'],
                    'id_estado_servicio_FK'  =>  $_POST['id_estado_servicio_FK']
                ];

            $arrayArtifacts   = isset($_POST['artifacts'])   ? $_POST['artifacts']   : []; //llega del js
            $arrayScategories = isset($_POST['scategories']) ? $_POST['scategories'] : []; //llega del js
            $arrayTechnicians = isset($_POST['technicians']) ? $_POST['technicians'] : []; //llega del js
            $arrayDetails     = isset($_POST['details'])     ? $_POST['details']     : []; //llega del js

            $arrayDetails[0]["is_software"] = ($arrayDetails[0]["is_software"] == "true") ? true : false;
            $arrayDetails[0]["is_hardware"] = ($arrayDetails[0]["is_hardware"] == "true") ? true : false;

            // dd($dataService);

            if (
                !empty($arrayArtifacts)
                && !empty($arrayScategories)
                && !empty($dataService)
                && !empty($arrayTechnicians)
                && !empty($arrayDetails)
            ) {

                $respService = $this->model->editService($dataService);

                $this->model->deleteArtService($_POST['id_servicio_PK']); //Quitar detalles
                $this->model->deleteCatService($_POST['id_servicio_PK']);
                $this->model->deleteTechService($_POST['id_servicio_PK']);

                $respArtService = $this->model->saveArtService(
                    $arrayArtifacts, //insercion al detalle artefacto
                    $_POST['id_servicio_PK']
                );

                $respCatService = $this->model->saveCatService(
                    $arrayScategories, //insercion al detalle categoria
                    $_POST['id_servicio_PK']
                );

                $respTechService = $this->model->saveTechService(
                    $arrayTechnicians,  //insercion al detalle Tecnico
                    $_POST['id_servicio_PK'],
                );

                $respDetService = $this->model->editDetService($arrayDetails, $_POST['id_servicio_PK']);
            } else {
                $respService        = false;
                $respArtService     = false;
                $respCatService     = false;
                $respTechService    = false;
                $respDetService     = false;
            }
        } else {
            $arrayResp = [
                'error' => true,
                'message' => "Error generando el servicio"
            ];
        }
        echo json_encode($arrayResp);
        return;
    }
}
