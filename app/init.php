<?php
// En orden
require_once 'providers/testing.php'; // 1. métodos para errores como dd()
require_once 'providers/iTL.php';     // 2. para fechas en español y con formato dependiendo la ubicación
require_once 'config/config.php';     // 3. constantes para facilitar urls
require_once 'core/Database.php';     // 4. crud y conexión con la bd
require_once 'core/Controller.php';   // 5. métodos para facilitar modelos y autenticaciones a los controladores
require_once 'providers/route.php';   // 6. métodos para que la vista se comunique con el controlador
