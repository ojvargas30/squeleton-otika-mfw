<!DOCTYPE html>
<html lang="es">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" lang="es" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta name="Author" lang="es" content="Óscar Javier Vargas Diaz, oscarjaviervargas@hotmail.com">
    <meta name="DC.identifier" lang="es" content="">
    <!--Aqui va la pagina Solumovil.............................-->
    <META http-equiv="Expires" lang="es" content="0">
    <!--ESTA NOSE PARA QUE ES.-->
    <meta name="Keywords" lang="es" content="Engativa,Colombia, Bogota,
	servicio tecnico, localidad, Quitar Cuenta Google,
	reparacion, celulares, pantalla, dañada, puerto de carga, tablets, baratos,
	flasheo de celulares o tablets, cambio de pantalla, cambio de repuestos.">
    <META http-equiv="PICS-Label" content='
	(PICS-1.1 "http://www.gcf.org/v2.5"
	labels on "1994.11.05T08:15-0500"
	until "1995.12.31T23:59-0000"
	for "http://w3.org/PICS/Overview.html"
	ratings (suds 0.5 density 0 color/hue 1))
 '>
    <!--Esto es para ayudar a los padres y a las escuelas a controlar los lugares a los
   que pueden acceder los niños en Internet, también facilita otros usos para las etiquetas,
  incluyendo firmas de código, privacidad, y gestión de los derechos de la propiedad
  intelectual.-->
    <META name="copyright" content="&copy; 2020 Solumovil Company.">
    <meta name="Description" lang="es" content="Pagina de servicio profesional
  enfocada en el mantenimiento y reparacion de celulares en la ciudad de Bogota-Colombia.
  Servicio tecnico de moviles.">
    <META name="date" content="19:05:09, sábado 29, febrero 2020 -05">
    <meta name="generator" content="HTML-KIT 2.9" />
    <meta name="language" content="es" />
    <meta name="revisit-after" content="1 month" />
    <meta name="robots" content="index, follow" />
    <meta name="application-name" content="servicio tecnico web de reparacion de celulares" />
    <meta name="encoding" charset="utf-8" />
    <meta http-equiv=»X-UA-Compatible». />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
    <meta name="organization" content="Solumovil Company" />
    <meta name="revisit" content="7" />
    <noscript>
        <meta http-equiv="refresh" content="60; url=https://www.youtube.com/watch?v=XyW1XiNBsaQ">
    </noscript>

    <!----------------------------------------------------------------------------------->
    <!----------------------------------------------------------------------------------->
    <!--TERMINA AQUI --------------------------------------------------------------META-->
    <!----------------------------------------------------------------------------------->


    <link rel="stylesheet" href="<?php echo CSS; ?>">

    <!-- Plantilla -->
    <link rel="stylesheet" href="<?php echo APP_CSS; ?>">
    <link rel="stylesheet" href="<?php echo COMPONENTS_CSS; ?>">
    <link rel="stylesheet" href="<?php echo CUSTOM_CSS; ?>">
    <link rel="stylesheet" href="<?php echo STYLE_CSS; ?>">

    <!-- DATATABLE -->
    <link rel="stylesheet" href="<?php echo DATATABLE_CSS; ?>">
    <link rel="stylesheet" href="<?php echo DATATABLE_B4; ?>">



    <!-- <link rel="stylesheet" href="<?php //echo B4;
                                        ?>"> -->

    <link rel="stylesheet" href="<?php echo FAWE; ?>">

    <!-- normalize -->
    <!-- <link rel="stylesheet" href="assets/css/normalize.css"> -->

    <!-- Favicon -->
    <!-- <link rel="Shortcut Icon" href="assets/img/1.ico" /> -->

    <!-- SWAL -->
    <!-- <link rel="stylesheet" href="assets\plugins\swal\dist\sweetalert2.min.css"> -->

    <!-- TOSTR -->
    <!-- <link rel="stylesheet" href="assets\plugins\toastr\toastr.min.css"> -->

    <!-- SELECT CON BUSCADOR -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/select2/select2.min.css"> -->

    <!-- FONTAWESOME -->
    <!-- <link rel="stylesheet" href="assets/font/css/all.css"> -->

    <!-- WaitMe -->
    <!-- <link rel="stylesheet" href="assets/plugins/waitMe/waitMe.min.css"> -->

    <!-- ANIMATE.CSS -->
    <!-- <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.compat.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/animate/animate.min.css"> -->
</head>

<body>
    <div class="loader"></div>
    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            
