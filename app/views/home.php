<div class="row">
    <div class="col-lg-12 pt-0">
        <h1><i class="fas fa-tachometer-alt fa-lg"></i> Reportes</h1>
        <p class=""> ¿De que deseas ponerte al tanto?</p>
        <hr />
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 p-2">
        <a class="text-decoration-none h-100 d-flex" href="#">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <i class="fas fa-bookmark fa-2x" aria-hidden="true"></i>
                    <hr />
                    <p class="card-title lead">Offer's</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 p-2">
        <a class="text-decoration-none h-100 d-flex" href="#">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <i class="fa fa-edit fa-2x" aria-hidden="true"></i>
                    <hr />
                    <p class="card-title lead">Blog's</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 p-2 ">
        <a class="text-decoration-none h-100 d-flex" href="#">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <i class="fa fa-image fa-2x" aria-hidden="true"></i>
                    <hr />
                    <p class="card-title lead">Gallery</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 p-2">
        <a class="text-decoration-none h-100 d-flex" href="#">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <i class="fa fa-envelope fa-2x" aria-hidden="true"></i>
                    <hr />
                    <p class="card-title lead">Enquiry</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 p-2">
        <a class="text-decoration-none h-100 d-flex" href="#">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <i class="fa fa-cart-plus fa-2x" aria-hidden="true"></i>
                    <hr />
                    <p class="card-title lead">Order's</p>
                </div>
            </div>
        </a>
    </div>

    <div class="col-xs-6 col-sm-6 col-md-3 col-lg-2 p-2">
        <a class="text-decoration-none h-100 d-flex" href="#" data-toggle="modal" data-target="#modelHELP">
            <div class="card p-3 shadow text-center border-0">
                <div class="card-body">
                    <i class="fa fa-question fa-2x" aria-hidden="true"></i>
                    <hr />
                    <p class="card-title lead">Support</p>
                </div>
            </div>
        </a>
    </div>
</div>



<!-- <div class="row">
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="card-statistic-4">
                <div class="align-items-center justify-content-between">
                    <div class="row ">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                            <div class="card-content">
                                <h5 class="font-15">New Booking</h5>
                                <h2 class="mb-3 font-18">258</h2>
                                <p class="mb-0"><span class="col-green">10%</span> Increase</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                            <div class="banner-img">
                                <img src="assets/img/banner/1.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="card-statistic-4">
                <div class="align-items-center justify-content-between">
                    <div class="row ">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                            <div class="card-content">
                                <h5 class="font-15"> Customers</h5>
                                <h2 class="mb-3 font-18">1,287</h2>
                                <p class="mb-0"><span class="col-orange">09%</span> Decrease</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                            <div class="banner-img">
                                <img src="assets/img/banner/2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="card-statistic-4">
                <div class="align-items-center justify-content-between">
                    <div class="row ">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                            <div class="card-content">
                                <h5 class="font-15">New Project</h5>
                                <h2 class="mb-3 font-18">128</h2>
                                <p class="mb-0"><span class="col-green">18%</span>
                                    Increase</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                            <div class="banner-img">
                                <img src="assets/img/banner/3.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3 col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="card-statistic-4">
                <div class="align-items-center justify-content-between">
                    <div class="row ">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pr-0 pt-3">
                            <div class="card-content">
                                <h5 class="font-15">Revenue</h5>
                                <h2 class="mb-3 font-18">$48,697</h2>
                                <p class="mb-0"><span class="col-green">42%</span> Increase</p>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pl-0">
                            <div class="banner-img">
                                <img src="assets/img/banner/4.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div> -->