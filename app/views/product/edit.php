<div class="row">
    <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4>Editar Producto</h4>
            </div>
            <div class="card-body">
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label"></label>
                    <div class="col-sm-9">
                        <input type="text" value="<?php echo $products[0]->modelo ?>" class="form-control" required="">
                        <div class="invalid-feedback">
                            What's your name?
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-3 col-form-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" value="<?php echo $products[0]->imei ?>" class="form-control" required="">
                        <div class="invalid-feedback">
                            Oh no! Email is invalid.
                        </div>
                    </div>
                </div>
                <div class="form-group mb-0 row">
                    <label class="col-sm-3 col-form-label">Message</label>
                    <div class="col-sm-9">
                        <textarea class="" cols="40" rows="6" required=""><?php echo $products[0]->caracteristicas; ?></textarea>
                        <div class="invalid-feedback">
                            What do you wanna say?
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-right">
                <button type="submit" class="btn btn-primary">Editar</button>
            </div>
        </div>
    </div>
</div>