<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h4>Productos</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped" id="table-1">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    #
                                </th>
                                <th>Modelo</th>
                                <th>Caracteristicas</th>
                                <th>Imei</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($products as $product) : ?>
                                <tr>
                                    <td><?php echo $product->id_artefacto_PK ?></td>
                                    <td><?php echo $product->modelo ?></td>
                                    <td><?php echo $product->caracteristicas ?></td>
                                    <td><?php echo $product->imei ?></td>
                                    <td>
                                        <a href="<?php echo route("product/edit/{$product->id_artefacto_PK}") ?>" class="btn btn-default">Editar</a>
                                        <!-- <a href="<?php //echo route() ?>" class="btn btn-default">Eliminar</a> -->
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>